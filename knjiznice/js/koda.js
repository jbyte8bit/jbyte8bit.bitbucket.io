
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function generiraj(){
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    ehrId = "";
    var ime;
    var priimek;
    var datumRojstva;
    var sistolicni;
    var diastolicni;
    
    
  // TODO: Potrebno implementirati
    if (stPacienta == 1) {
        ime = "Janez";
        priimek = "Novak";
        datumRojstva = "1988-03-03"
        sistolicni = 115;
        diastolicni = 78;
        zapisi(ime, priimek, datumRojstva, sistolicni, diastolicni);
    }
    
    if (stPacienta == 2) {
        ime = "Polde";
        priimek = "Polh";
        datumRojstva = "1959-01-01"
        sistolicni = 160;
        diastolicni = 93
        zapisi(ime, priimek, datumRojstva, sistolicni, diastolicni);
    }
    
    if (stPacienta == 3) {
        ime = "Nada";
        priimek = "Breza";
        datumRojstva = "2006-02-02"
        sistolicni = 125;
        diastolicni = 75;
        zapisi(ime, priimek, datumRojstva, sistolicni, diastolicni);
    }
    return ehrId;
}

function zapisi(ime, priimek, datum, sistolicni, diastolicni) {
	sessionId = getSessionId();
	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: datum,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#preberiEhrIdZaBMI").append("<option value=''></option><option value='" + ehrId + "'>" + ime + "</option>");
	                    $("#preberiObstojeciEHR").append("<option value=''></option><option value='" + ehrId + "'>"+ ime + "</option>");
	                    $("#preberiObstojeciVitalniZnak").append("<option value=''></option><option value='" + ehrId + "'>" + ime + "</option>");
	                    $("#generiraniPodatki").append("<span>" + ime + " " + ehrId + "</span><br>");
	
	                	
	                	    $.ajaxSetup({
	                		    headers: {"Ehr-Session": sessionId}
	                		});
	                		var podatki = {
	                		    "ctx/language": "en",
	                		    "ctx/territory": "SI",
	                		    "vital_signs/height_length/any_event/systolic": sistolicni,
	                		    "vital_signs/body_weight/any_event/diastolic": diastolicni
	                		};
	                		var parametriZahteve = {
	                		    ehrId: ehrId,
	                		    templateId: 'Vital Signs',
	                		    format: 'FLAT',
	                		    commiter: "user"
	                		};
	                		$.ajax({
	                		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
	                		    type: 'POST',
	                		    async: false,
	                		    contentType: 'application/json',
	                		    data: JSON.stringify(podatki),
	                		    success: function (res) {
	                                console.log("USPEŠNO");
	                		    },
	                		    error: function(err) {
	                		    	console.log("Napaka");
	                		    }
	                		});
	
	                    }
	                },
	            error: function(err) {
	            	console.log("Napaka.");
	            }
	        });
	    }
	});
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
kreirajEHRzaBolnika = function() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
function preberiEhrId() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function dodajMeritveKrvnegaTlaka() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var sistolicni = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicni = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if (ehrId == "" || sistolicni == "" || diastolicni == ""|| ehrId.trim().length == 0) {
		$("#dodajMeritveKrvnegaTlakaSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveKrvnegaTlakaSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveKrvnegaTlakaSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function preberiMeritveKrvnegaTlaka() {
	sessionId = getSessionId();
	
	var ehrId = $("#dodajEhr").val();
	
	if (ehrId == "" || ehrId.trim().length == 0) {
		$("#preberiEhrIdSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
		    type: 'GET',
		    headers: {
		        "Ehr-Session": sessionId
		    },
		    success: function (res) {
		    	 $("#rezultatMeritveKrvnegaTlaka").append("Sistolični krvni tlak: " + res[0].systolic + res[0].unit + "<br>");
		    	 $("#rezultatMeritveKrvnegaTlaka").append("Diastolični krvni tlak: " + res[0].diastolic + res[0].unit + "<br>");
		    }
		    
		});
		
	}
}

function preberiOsebnePodatke() {
	sessionId = getSessionId();
	
	var ehrId = $("#dodajEhr1").val();
	var searchData = [
    {key: "ehrId", value: ehrId}
];
	
	if (ehrId == "" || ehrId.trim().length == 0) {
		$("#preberiEhrIdSporocilo1").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajaxSetup({
		    headers: {
		        "Ehr-Session": sessionId
		    }
		});		
		$.ajax({
		    url: baseUrl + "/demographics/party/query",
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(searchData),
		    success: function (res) {
		        for (i in res.parties) {
		            var party = res.parties[i];
		            $("#rezultatOsebnihPodatkov").append(party.firstNames + ' ' + party.lastNames + '<br>');
		        }
		    }
		});
	}
}



$(document).ready(function() {

  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });
  
  $('#preberiObstojeciVitalniZnak').change(function() {
	$("#dodajMeritveKrvnegaTlakaSporocilo").html("");
	var podatki = $(this).val().split(",");
	$("#dodajVitalnoEHR").val(podatki[0]);
	$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[1]);
	$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[2]);
    });
    
  $('#preberiEhrId1').change(function() {
	$("#preberiEhrIdSporocilo1").html("");
	$("#dodajEhr1").val($(this).val());
    });
    
  $('#preberiEhrId').change(function() {
	$("#preberiEhrIdSporocilo").html("");
	$("#dodajEhr").val($(this).val());
    });
});
